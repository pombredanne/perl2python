#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment
# from lab week 5 digits.pl

while($input = <STDIN>){
    $input =~ s/[0-4]/</g;
    $input =~ s/[6-9]/>/g;
    print "$input";
}
