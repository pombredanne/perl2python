#!/usr/bin/perl

# written by andrewt@cse.unsw.edu.au September 2013
# as a starting point for COMP2041/9041 assignment 
# http://cgi.cse.unsw.edu.au/~cs2041/13s2/assignments/perl2python

@code = <>;

foreach $line (@code) {
    # skim read to set flags and translating certain phrases
	if($line =~ /<STDIN>/ || $line =~ /ARGV/){	
		$sys = 1;
		$line =~ s/\@ARGV/sys.argv[1:]/g;
	}
	if($line =~ /(\d)+\.\.(\d)+/){
	    $range1 = $1;
	    $range2 = $2+1;
	    $line =~ s/\d+\.\.\d+/xrange($range1, $range2)/g;
	}
	if($line =~ /<>/){
	    $fileinput = 1;
	}
	if($line =~ /0\.\.\$#ARGV/){
	    $line =~ s/0\.\.\$#ARGV/xrange(len(sys.argv)-1)/g;
	}
	if($line =~ /\$ARGV\[(.*)\]/){
	    $index = $1;
	    $index =~ s/\$//g;
	    $line =~ s/ARGV\[.*\]/sys.argv[$index+1]/g;
	}
	if($line =~ /(\$[^\s]*)\s*%|[<>=]+\s*[\d]+/){
	    $float = 1;
	    $floatvariable = $1;    
    }
    if($line =~ /open\s(.*),\s*[\"<>]*([^";]*)[\";]*/){
		$open{$1} = $2;
    }
    if($line =~ /\$([^\s]*){.*}.*;/g){
		$hash = 1;
		$hashes{$1} = 1;
    }
    if($line =~ /\$[1-9]/){
		$line =~ s/\$([0-9])/m.group($1)/g;
    }
}



foreach $line (@code) {
	if ($line =~ /^#!/ && $line eq $code[0]) {
		# TRANSLATE #! LINE 
		print "#!/usr/bin/python2.7 -u\n";
		
		if ($sys){
			# stdin / @ARGV case
			print "import sys\n";
			$sys = 0;
		}
		if ($fileinput){
		    # fileinput case
		    print "import fileinput, re\n";
		    $fileinput = 0;
		}
	}

	
	elsif ($line =~ /^\s*#/ || $line =~ /^\s*$/) {
		# Blank & comment lines can be passed unchanged
		print $line;
	}


	elsif($line =~ /^\s*[^#]*/ && $hash){
		
		if($hash){
			# INITIALIZED HASH TABLES
			foreach $element (keys %hashes){
				print "\n$element = {}";
			}
			print "\n\n";
			$hash = 0;
		}
	}

	
	elsif ($line =~ /^\s*print\s*(.*)/){
	    # TRANSLATE 'PRINT' STATEMENT
		$content = $1;

		if($content =~ /^\"[^\$]*\"/){
			# string  (print"...\n";)
			$line =~ s/[\$;]*|\\n//g;
			$content=~ s/;\s*$//g;
			if($content =~ /[Ee][Nn][Tt][Ee][Er]/ && $line =~ /^print/){
			    $line =~ s/print.*$/sys.stdout.write($content)/g;
			}
		}
		elsif($content =~ /^"[\$]*[^\s]*"/){
			# simple variable  (print $...;)
			$line =~ s/[\"\$;]*|\\n//g;
		}
		elsif($content =~ /^(.*),\s*\"\\n\"/){
		    # (print ...., "\n";)
			$join = $1;
			if($join =~ /join\s*\((.*)\s*,\s*(.*)\)/){
				$space = $1;
				$joincontent = $2;
				$line =~ s/join\s*\(.*\)/$space.join($joincontent)/g;
				$line =~ s/,*\s*\"\\n\";//g;
			}
			else{
				$line =~ s/[\$;]//g;
				$line =~ s/,*\s*\"\\n\"//g;
			}
		}
        elsif($content =~ /^"\$([^\s]*)\s+([^\$]*)\\n"/){
            # (print $...., "...";)
            $word1 = $1;
            $word2 = $2;

            $line =~ s/".*";/$word1, "$word2"/g;
        }
        else{
            # (print "%...%..")
            $content =~ s/[\";]*|\\n//g;
            @words = split(' ',$content);
            $line =~ s/\\n|;//g;
            
            foreach $word (@words){
                if($word =~ /\$[^\s]*{.*}/){
                    $variables .= "$word,";
                    $word =~ s/\$|{.*}//g;
                    $line =~ s/\$$word\{[^\s]*\}/%s/g;      
                }
            }
            $variables =~ s/,$//;
            $variables =~ s/\$//g;
            $variables =~ s/{/[/g;
            $variables =~ s/}/]/g;
            $line =~ s/\n/%($variables)\n/;
        }
        print $line;
	}




	elsif ($line =~ /^\s*(my)*\s*\$([\w\d]*)\s*=\s*([^;^~]*)[\s;]*$/){
		# TRANSLATE VARIABLE INITIALIZATION
		$content = $3;
		$line =~ s/[\$;]//g;

		if ($content eq "<STDIN>"){
		    # <STDIN> case
			$line =~ s/<STDIN>/sys.stdin.readline()/g;			 
		    if($float){
                $line =~ s/=\s*/= float(/g;
                $line =~ s/\n/)\n/g;
            }
		}
		print $line;
	}


	elsif ($line =~ /^(\s*)if\s*\((.*)\)\s*{/){
		# TRANSLATE 'IF' STATEMENTS
		$content = $2;
		$space = $1;
		if($content =~ /\$(.*)\s*=~\s\/*(.*)\//){
		    # '=~' case
			$variable = $1;
			$regex = $2;
			$line =~ s/if.*$/m = re.match(r'$regex', $variable)/;
			print $line;
			$line =~ s/^.*$/$space\if m:/;
			print $line;
		}
		else{
		    # normal case
			$line =~ s/[\(\$]//g;
			$line =~ s/\)\s*{/:/g;
			$line =~ s/eq/==/g;
			print $line;
		}

	}


	elsif ($line =~ /else/){
	    # TRANSLATE 'ELSE' STATEMENTS
	    $line =~ s/}\s*//g;
	    $line =~ s/\s*{/:/g;
	    print $line;

	}


	elsif ($line =~ /elsif/){
		# TRANSLATE 'ELSIF' STATEMENTS
        $line =~ s/}\s*//g;
		$line =~ s/[\(\$]//g;
		$line =~ s/\)\s*{/:/g;
		$line =~ s/eq/==/g;
		print $line; 
	}

	
	elsif ($line =~ /^\s*while\s*\(([^(^).]*)\)\s*{/){
		# TRANSLATE 'WHILE' STATEMENTS
        $content = $1;

        if($content =~ /<>/){
            # <> case
            $line =~ s/while\s*\(\$/for /;
            $line =~ s/=.*/in fileinput.input():/;
        }
        elsif($content =~ /<STDIN>/){
            # <STDIN> case
            $line =~ s/while\s*\(\$/for /;
            $line =~ s/=.*/in sys.stdin:/;
        }
        elsif($content =~ /<(.*)>/){
            # <F> case
            $symbol = $1;
            $line =~ s/while\s*\(\$/for /;
            $line =~ s/=.*/in open("$open{$symbol}"):/;
        }
        else{
            # normal case
		    $line =~ s/[\(\$]//g;
		    $line =~ s/\)\s*{/:/g;
		    $line =~ s/eq/==/g;
		    $line =~ s/lt/</g;
		    $line =~ s/gt/>/g;
		    
		}
        print $line;
	}
	

	
	elsif ($line =~ /^\s*chomp\s+\$(.*);/){
		# TRANSLATE 'CHOMP' COMMAND
		$variable = $1;
		$line =~ s/[^\s].*//g;
		chomp $line;
		print "$line$variable = $variable.rstrip()\n";
		

	}
	elsif ($line =~ /^\s*last\s*;/){
		# TRANSLATE 'LAST' COMMAND
		$line =~ s/last\s*;/break/g;
		print $line;
		
	}

	elsif ($line =~ /^\s*foreach\s*.*\((.*)\)\s*{/){
	    # TRANSLATE 'FOREACH' LOOP
	    $content = $1;
	    if($content=~ /keys\s*%/){
	        # hash tables case
	        if($content =~ /sort/){
	            # sorted case
	            $line =~ s/sort\s*/sorted(/;
	            $line =~ s/\s*{/:/g;   
            }
            else{
                $line =~ s/[\(\)]*//g;
            }
            $line =~ s/each*|\$*//g;
	        $line =~ s/\(/in /;
	        $line =~ s/\)\s*{/:/g;
            $line =~ s/keys\s*%([^\(\)]*)/$1.keys()/g;
            print $line;
        }
	    else{
	        # normal case
	        $line =~ s/each*|\$*//g;
	        $line =~ s/\(/in /;
	        $line =~ s/\)\s*{/:/g;
	        $line =~ s/\@//g;
	        print "$line";
	    }
	}


	elsif ($line =~ /\$(.*)\s*=~\s*s\/(.*)\/(.*)\/g*/){
	    # TRANSLATE 's///' COMMAND
	    $variable = $1;
	    $phrase = $2;
	    $replace = $3;
	  	$line =~ s/[\$;]//g;
	  	$line =~ s/~\s*s\/.*\/.*\/g*/ re.sub(r'$phrase', '$replace', $variable)/g;
	    print $line;
	}


    elsif ($line =~ /(\s*)(\$.*)\+\+|(.*)--/){
        # TRANSLATE INCREMENTATION ++ OR DECREMENTATION --
        $variable = $2;
        $space = $1;
        $variable =~ s/\$//g;
        
        if($variable =~ /(.*){[^{}]*]}/){
        	# hash table case
			print "$space\if $2 in $1:\n";
			$variable =~ s/{/[/g;
       		$variable =~ s/}/]/g;
			print "$space$space$variable += 1\n";
			print "$space\eeelse:\n";
			print "$space$space$variable = 1\n";
        }
        elsif($variable =~ /(.*){(.*)}{(.*)}/){
            # 2D hash table case
            $var1 = $1;
            $var2 = $2;
            $var3 = $3;
            print "$space\if $var2 not in $var1:\n";
            $variable =~ s/{/[/g;
       		$variable =~ s/}/]/g;
       		$variable2 = $variable;
            $variable2 =~ s/\[[^\[\]]*\]\s*$//g;
            print "$space$space$variable2 = {}\n";
            print "$space\if $var3 in $variable2:\n";
            print "$space$space$variable += 1\n";
            print "$space\eeelse:\n";
			print "$space$space$variable = 1\n";
        }
        else{
            # normal case
		    $line =~ s/\$//g;
		    $line =~ s/\+\+\s*;/ += 1/g;
		    $line =~ s/--\s*;/ -= 1/g;
		    print $line;
		}
    }
    

    elsif ($line =~ /^\s*open\s.*,.*;/){
		# TRANSLATE 'OPEN' COMMAND
    }
    

    elsif ($line =~ /^\s*close\s.*;/){
		# TRANSLATE 'CLOSE COMMAND
    }


	elsif ($line =~ /^\s*\$[^\s]*{(.*)}\s*=\s*(.*);/){
	    # TRANSLATE HASH TABLES
		$line =~ s/\$//g;
		$line =~ s/{/[/g;
		$line =~ s/}/]/g;
		print $line;
    }
    

    elsif ($line =~ /^\s*@([^\s]*)\s*=\s*split\s*\/(.*)\/\s*,\s*\$(.*);/){
        # TRANSLATE 'SPLIT COMMAND
        $output = $1;
        $regex = $2;
        $input = $3;
        $line =~ s/@.*/$output = $input.split('$regex')\n/g;
        print $line;  
    }


    elsif($line =~ /([\s]*)\$(.*)\s*=~\s*\/(.*)\//){
        # TRANSLATE 'IF MATCH' (=~)
		$space = $1;
		$variable = $2;
		$regex = $3;
		if($line =~ /or\s*next/){
		    # 'or next' case
	        $next = 1;
		}
		$line =~ s/\$.*/m = re.match(r'$regex', $variable)/;
		print $line;
		if($next){
		    $line =~ s/^.*$/$space\if not m:/;
		    print $line;
		    $line =~ s/^.*$/$space$space\cccontinue/;
		    print $line;
		}
	}
	
	
    elsif ($line =~ /^\s*\$.*\.=.*$/){
        # TRANSLATE (.=) 
        $line =~ s/\$|;//g;
        $line =~ s/\./+/g;
        print $line;

    }
    
    
    elsif ($line =~ /^\s*return.*/){
        # TRANSLATE RETURN COMMAND
        $line =~ s/\$|;//g;
        print $line;
    }
    
    
	elsif ($line =~ /^\s*}\s*$/){
		# TRANSLATE CLOSING CURLY BRACKETS '}'
	}


	else {
		# Lines we can't translate are turned into comments
		print "#$line\n";
	}
}
