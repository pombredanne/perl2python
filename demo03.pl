#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment
# from http://sandbox.mc.edu/~bennet/perl/leccode/while_pl.html

my $a = 5;
while ($a > 0) {
    print "$a ";
    $a--;
}
print "\n";
