#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment

while ($line = <>) {
    chomp $line;
    $line =~ s/A/a/g;
    $line =~ s/E/e/g;
    $line =~ s/I/i/g;
    $line =~ s/O/o/g;
    $line =~ s/U/u/g;
    $line =~ s/a/A/g;
    $line =~ s/e/E/g;
    $line =~ s/i/I/g;
    $line =~ s/o/O/g;
    $line =~ s/u/U/g;
    print "$line\n";
}

