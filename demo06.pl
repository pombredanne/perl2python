#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment

$number = 0;
while ($number >= 0) {
    print "Enter number:\n";
    $number = <STDIN>;
    if ($number > 100) {
        print "BIG\n";
    }
    else {
        print "SMALL\n";
    }
}

