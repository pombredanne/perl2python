#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment
# from lab week 6 pastes.pl

foreach $file (@ARGV){
	open "file","$file";
	
	$output ="NULL";
	while ($line = <file>){
		$line =~ s/\n//;
		
		if ($output eq "NULL"){
			$output = $line;
		} 
		else {
			$output = $output."\t".$line;
		}
	}

	print "$output\n";
}
