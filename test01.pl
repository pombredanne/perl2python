#!/usr/bin/perl

# written by Andrew Gosali z3438251
# for testing perl2python.pl COMP2041 S2 2013 assignment
# testing if statements

$a = 10;

if ($a == 10){
    print "it is 10\n";
}
else{
    print "it is not 10\n";
}
