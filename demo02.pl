#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment
# from http://www.perl.com/pub/2000/10/begperl1.html

$a = 5;
$b = $a + 10;       # $b is now equal to 15.
$c = $b * 10;       # $c is now equal to 150.
$a = $a - 1;        # $a is now 4, and algebra teachers are cringing.
    
print "$a $b $c $d\n";
