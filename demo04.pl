#!/usr/bin/perl

# by Andrew Gosali z3438251
# for perl2python COMP2041 S2 2013 assignment
# demo on hash tables

$h{a} = 1;
$h{b} = 2;
$h{c} = 3;

foreach $element (sort keys %h){
    print "$element\n";
}
